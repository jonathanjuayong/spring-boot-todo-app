package com.jonathanjuayong.springboottodoapp.todo

import com.jonathanjuayong.springboottodoapp.SpringBootTodoAppApplicationTests
import com.jonathanjuayong.springboottodoapp.repositories.TodoRepository
import com.jonathanjuayong.springboottodoapp.repositories.UserRepository
import com.jonathanjuayong.springboottodoapp.serviceImpl.TodoServiceImpl
import com.jonathanjuayong.springboottodoapp.utils.Constants
import com.jonathanjuayong.springboottodoapp.utils.EntityGenerator
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class TodoServiceImplTest: SpringBootTodoAppApplicationTests() {
    @Autowired
    private lateinit var todoRepository: TodoRepository

    @Autowired
    private lateinit var todoServiceImpl: TodoServiceImpl

    @Autowired
    private lateinit var userRepository: UserRepository

    @BeforeEach
    fun cleanUp() {
        todoRepository.deleteAll()
        userRepository.deleteAll()
    }

    private fun initializeTodoData() =
        EntityGenerator.initializeUserRelatedData(
            userRepository,
            EntityGenerator::createRandomTodo
        )

    @Test
    fun `get all user todos should return all todos by user given a valid id`() {
        val (user, todo1) = initializeTodoData()
        val todo2 = EntityGenerator.createRandomTodo(user)
        assertEquals(0, todoServiceImpl.getAllTodosByUser(user.id).count())
        todoRepository.saveAll(listOf(todo1, todo2))
        assertEquals(2, todoServiceImpl.getAllTodosByUser(user.id).count())
    }

    @Test
    fun `get all user todos should fail given an invalid id`() {
        val exception = assertFailsWith<ResponseStatusException> {
            todoServiceImpl.getAllTodosByUser(Constants.INVALID_ID)
        }
        val expectedError = "404 NOT_FOUND \"User with id: ${Constants.INVALID_ID} does not exist\""
        assertEquals(expectedError, exception.message)
    }

    @Test
    fun `get todo by id should return a todo given a valid id`() {
        val (_, todo) = initializeTodoData()
        val savedTodo = todoRepository.save(todo)
        val todoFromServiceImpl = todoServiceImpl.getTodoById(savedTodo.id)

        assertEquals(savedTodo.id, todoFromServiceImpl.id)
        assertEquals(savedTodo.title, todoFromServiceImpl.title)
        assertEquals(savedTodo.description, todoFromServiceImpl.description)
    }

    @Test
    fun `get todo by id should fail given an invalid id`() {
        val exception = assertFailsWith<ResponseStatusException> {
            todoServiceImpl.getTodoById(Constants.INVALID_ID)
        }
        val expectedError = "404 NOT_FOUND \"Todo with id: ${Constants.INVALID_ID} does not exist\""
        assertEquals(expectedError, exception.message)
    }

    @Test
    fun `create todo should return created object given valid data`() {
        val (user, todo) = initializeTodoData()
        val createdTodo = todoServiceImpl.createTodo(user.id, todo)
        assertEquals(todo.user.id, createdTodo.user.id)
        assertEquals(todo.title, createdTodo.title)
        assertEquals(todo.description, createdTodo.description)
    }

    @Test
    fun `create todo should fail given duplicate title`() {
        val (user, todo1) = initializeTodoData()
        val todo2 = todo1.copy(description = "a different description but same title")
        todoServiceImpl.createTodo(user.id, todo1)
        val exception = assertFailsWith<ResponseStatusException> {
            todoServiceImpl.createTodo(user.id, todo2)
        }
        val expectedError = "409 CONFLICT \"Todo with title: ${todo2.title} already exists\""
        assertEquals(expectedError, exception.message)
    }

    @Test
    fun `update todo should return updated todo given valid id`() {
        val (_, todo) = initializeTodoData()
        val savedTodo = todoRepository.save(todo)
        val newTodo = savedTodo.copy(
            title = "This is a new todo",
            description = "This is also a new description"
        )
        val updatedTodo = todoServiceImpl.updateTodo(savedTodo.id, newTodo)
        assertEquals(newTodo.id, updatedTodo.id)
        assertEquals(newTodo.title, updatedTodo.title)
        assertEquals(newTodo.description, updatedTodo.description)
        assertEquals(newTodo.user.id, updatedTodo.user.id)
    }

    @Test
    fun `update todo should fail given an invalid id`() {
        val (_, todo) = initializeTodoData()
        val exception = assertFailsWith<ResponseStatusException> {
            todoServiceImpl.updateTodo(Constants.INVALID_ID, todo)
        }
        val expectedError = "404 NOT_FOUND \"Todo with id: ${Constants.INVALID_ID} does not exist\""
        assertEquals(expectedError, exception.message)
    }

    @Test
    fun `delete todo should remove todo given valid id`() {
        val (_, todo) = initializeTodoData()
        val savedTodo = todoRepository.save(todo)
        assertEquals(1, todoRepository.count())
        todoServiceImpl.deleteTodo(savedTodo.id)
        assertEquals(0, todoRepository.count())
    }

    @Test
    fun `delete todo should fail given invalid id`() {
        val exception = assertFailsWith<ResponseStatusException> {
            todoServiceImpl.deleteTodo(Constants.INVALID_ID)
        }
        val expectedError = "404 NOT_FOUND \"Todo with id: ${Constants.INVALID_ID} does not exist\""
        assertEquals(expectedError, exception.message)
    }
}