package com.jonathanjuayong.springboottodoapp.serviceImpl

import com.jonathanjuayong.springboottodoapp.entities.Todo
import com.jonathanjuayong.springboottodoapp.repositories.TodoRepository
import com.jonathanjuayong.springboottodoapp.repositories.UserRepository
import com.jonathanjuayong.springboottodoapp.services.TodoService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class TodoServiceImpl(
    private val todoRepository: TodoRepository,
    private val userRepository: UserRepository
) : TodoService {
    override fun getAllTodosByUser(userId: Long): List<Todo> =
        if (userRepository.existsById(userId))
            todoRepository.getAllUserTodos(userId)
        else
            throw ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "User with id: $userId does not exist"
            )


    override fun getTodoById(id: Long): Todo =
        todoRepository.findById(id).orElseThrow {
            ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "Todo with id: $id does not exist"
            )
        }

    override fun createTodo(userId: Long, body: Todo): Todo {
        if (todoRepository.isTitleDuplicate(body.title)) {
            throw ResponseStatusException(
                HttpStatus.CONFLICT,
                "Todo with title: ${body.title} already exists"
            )
        }

        val user = userRepository.findById(userId).orElseThrow {
            ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "User with id: $userId does not exist"
            )
        }
        return todoRepository.save(
            body.copy(
                user = user
            )
        )
    }

    override fun updateTodo(id: Long, body: Todo): Todo {
        val originalTodo = todoRepository.findById(id).orElseThrow {
            ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "Todo with id: $id does not exist"
            )
        }
        return todoRepository.save(
            body.copy(
                id = originalTodo.id,
                dateCreated = originalTodo.dateCreated
            )
        )
    }

    override fun deleteTodo(id: Long) {
        val todo = todoRepository.findById(id).orElseThrow{
            ResponseStatusException(HttpStatus.NOT_FOUND, "Todo with id: $id does not exist")
        }
        todoRepository.delete(todo)
    }
}