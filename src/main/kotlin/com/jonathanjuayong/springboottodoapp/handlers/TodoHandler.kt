package com.jonathanjuayong.springboottodoapp.handlers

import com.jonathanjuayong.springboottodoapp.entities.Todo
import com.jonathanjuayong.springboottodoapp.services.TodoService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/")
class TodoHandler(private val todoService: TodoService) {
    @GetMapping("todos/{userId}")
    fun getAllTodosByUser(@PathVariable("userId") userId: Long) =
        todoService.getAllTodosByUser(userId)

    @GetMapping("todo/{id}")
    fun getTodoById(@PathVariable("id") id: Long) =
        todoService.getTodoById(id)

    @PostMapping("todos/{userId}")
    fun createTodo(@PathVariable("userId") userId: Long, @RequestBody body: Todo) =
        todoService.createTodo(userId, body)

    @PutMapping("todo/{id}")
    fun updateTodo(@PathVariable("id") id: Long, @RequestBody body: Todo) =
        todoService.updateTodo(id, body)

    @DeleteMapping("todo/{id}")
    fun deleteTodo(@PathVariable("id") id: Long) =
        todoService.deleteTodo(id)
}