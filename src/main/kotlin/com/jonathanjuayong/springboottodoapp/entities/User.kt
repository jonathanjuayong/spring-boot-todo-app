package com.jonathanjuayong.springboottodoapp.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity
@Table(name = "users")
data class User(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @Column(
        nullable = false,
        updatable = true,
        name = "name"
    )
    val name: String = "",

    @Column(
        nullable = false,
        updatable = true,
        name = "email"
    )
    val email: String = "",

    @JsonIgnoreProperties(value = ["user"], allowSetters = true)
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    val todos: List<Todo> = listOf()
)