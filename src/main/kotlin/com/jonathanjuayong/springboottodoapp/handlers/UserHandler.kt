package com.jonathanjuayong.springboottodoapp.handlers

import com.jonathanjuayong.springboottodoapp.entities.User
import com.jonathanjuayong.springboottodoapp.services.UserService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/")
class UserHandler(private val userService: UserService) {
    @GetMapping("users")
    fun getAllUsers() =
        userService.getAllUsers()

    @GetMapping("users/{id}")
    fun getUserById(@PathVariable("id") id: Long) =
        userService.getUserById(id)

    @PostMapping("users")
    fun createUser(@RequestBody user: User) =
        userService.createUser(user)

    @PutMapping("users/{id}")
    fun updateUser(@PathVariable("id") id: Long, @RequestBody body: User) =
        userService.updateUser(id, body)

    @DeleteMapping("users/{id}")
    fun deleteUserById(@PathVariable("id") id: Long) =
        userService.deleteUser(id)
}