package com.jonathanjuayong.springboottodoapp.utils

object Constants {
    const val INVALID_ID: Long = -1
}