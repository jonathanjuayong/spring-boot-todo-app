package com.jonathanjuayong.springboottodoapp.todo

import com.fasterxml.jackson.databind.ObjectMapper
import com.jonathanjuayong.springboottodoapp.SpringBootTodoAppApplicationTests
import com.jonathanjuayong.springboottodoapp.repositories.TodoRepository
import com.jonathanjuayong.springboottodoapp.repositories.UserRepository
import com.jonathanjuayong.springboottodoapp.utils.EntityGenerator
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*

@AutoConfigureMockMvc
class TodoServiceIntegrationTest : SpringBootTodoAppApplicationTests() {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var todoRepository: TodoRepository

    @Autowired
    private lateinit var userRepository: UserRepository

    @BeforeEach
    fun setUp() {
        todoRepository.deleteAll()
        userRepository.deleteAll()
    }

    private fun initializeTodoData() =
        EntityGenerator.initializeUserRelatedData(
            userRepository,
            EntityGenerator::createRandomTodo
        )

    @Test
    fun `get all user todos should return all user todos`() {
        val (user, todo1) = initializeTodoData()
        val todo2 = todo1.copy(
            title = "This is my new todo",
            description = "This is the description for my new todo"
        )
        todoRepository.saveAll(listOf(todo1, todo2))
        mockMvc.get("/api/todos/${user.id}") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            jsonPath("$[0].user.id") { value(user.id) }
            jsonPath("$[1].user.id") { value(user.id) }
        }
    }

    @Test
    fun `get todo by id should return todo object`() {
        val (_, todo) = initializeTodoData()
        val savedTodo = todoRepository.save(todo)
        mockMvc.get("/api/todo/${savedTodo.id}") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            jsonPath("$.id") { value(savedTodo.id) }
        }
    }

    @Test
    fun `create todo should return created todo`() {
        val (user, todo) = initializeTodoData()
        mockMvc.post("/api/todos/${user.id}") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(todo)
        }.andExpect {
            status { isOk() }
            jsonPath("$.user.id") { value(user.id) }
            jsonPath("$.title") { value(todo.title) }
            jsonPath("$.description") { value(todo.description) }
        }
    }

    @Test
    fun `updated todo should return updated todo`() {
        val (_, todo) = initializeTodoData()
        val savedTodo = todoRepository.save(todo)
        val newBody = savedTodo.copy(
            title = "This is my new todo",
            description = "This is the description for my new todo"
        )

        mockMvc.put("/api/todo/${savedTodo.id}") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(newBody)
        }.andExpect {
            status { isOk() }
            jsonPath("$.id") { savedTodo.id }
            jsonPath("$.title") { newBody.title }
            jsonPath("$.description") { newBody.description }
        }
    }

    @Test
    fun `delete todo should remove todo`() {
        val (_, todo) = initializeTodoData()
        val savedTodo = todoRepository.save(todo)

        mockMvc.delete("/api/todo/${savedTodo.id}") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            content { string("") }
        }
    }
}