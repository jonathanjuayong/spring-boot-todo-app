package com.jonathanjuayong.springboottodoapp.user

import com.jonathanjuayong.springboottodoapp.SpringBootTodoAppApplicationTests
import com.jonathanjuayong.springboottodoapp.repositories.UserRepository
import com.jonathanjuayong.springboottodoapp.serviceImpl.UserServiceImpl
import com.jonathanjuayong.springboottodoapp.utils.Constants
import com.jonathanjuayong.springboottodoapp.utils.EntityGenerator
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class UserServiceImplTest: SpringBootTodoAppApplicationTests() {
    @Autowired
    private lateinit var userServiceImpl: UserServiceImpl

    @Autowired
    private lateinit var userRepository: UserRepository

    @BeforeEach
    private fun cleanUp() {
        userRepository.deleteAll()
    }

    @Test
    fun `get all users should return all users`() {
        val users = (1..5).map {
            EntityGenerator.createRandomUser()
        }
        userRepository.saveAll(users)
        assertEquals(5, userServiceImpl.getAllUsers().size)
    }

    @Test
    fun `get user by id should return user given a valid id`() {
        val user = EntityGenerator.createRandomUser()
        val savedUser = userRepository.save(user)
        val userFromServiceImpl = userServiceImpl.getUserById(savedUser.id)
        assertEquals(savedUser.id, userFromServiceImpl.id)
        assertEquals(savedUser.name, userFromServiceImpl.name)
        assertEquals(savedUser.email, userFromServiceImpl.email)
    }

    @Test
    fun `get user by id should fail given an invalid id`() {
        val exception = assertFailsWith<ResponseStatusException> {
            userServiceImpl.getUserById(Constants.INVALID_ID)
        }

        val expectedError = "404 NOT_FOUND \"User with id: ${Constants.INVALID_ID} does not exist\""

        assertEquals(expectedError, exception.message)
    }

    @Test
    fun `create user should return created user given valid data`() {
        val user = EntityGenerator.createRandomUser()
        val createdUser = userServiceImpl.createUser(user)
        assertEquals(user.id, createdUser.id)
        assertEquals(user.name, createdUser.name)
        assertEquals(user.email, createdUser.email)
    }

    @Test
    fun `create user should fail given duplicate email from existing user`() {
        val user1 = EntityGenerator.createRandomUser()
        val user2 = user1.copy(name = "max")
        userRepository.save(user1)
        val exception = assertFailsWith<ResponseStatusException> {
            userServiceImpl.createUser(user2)
        }
        val expectedError = "409 CONFLICT \"Email ${user2.email} already exists\""
        assertEquals(expectedError, exception.message)
    }

    @Test
    fun `update user should return updated user given valid data`() {
        val user = EntityGenerator.createRandomUser()
        val createdUser = userRepository.save(user)
        val newBody = createdUser.copy(
            name = "jjuayong",
            email = "mytestemail@gmail.com"
        )
        val updatedUser = userServiceImpl.updateUser(createdUser.id, newBody)
        assertEquals(newBody.id, updatedUser.id)
        assertEquals(newBody.name, updatedUser.name)
        assertEquals(newBody.email, updatedUser.email)
    }

    @Test
    fun `update user should fail given invalid id`() {
        val body = EntityGenerator.createRandomUser()
        val exception = assertFailsWith<ResponseStatusException> {
            userServiceImpl.updateUser(Constants.INVALID_ID, body)
        }
        val expectedError = "404 NOT_FOUND \"User with id: ${Constants.INVALID_ID} does not exist\""
        assertEquals(expectedError, exception.message)
    }

    @Test
    fun `delete user should work`() {
        val user = EntityGenerator.createRandomUser()
        userRepository.save(user)
        assertEquals(1, userRepository.count())
        userServiceImpl.deleteUser(user.id)
        assertEquals(0, userRepository.count())
    }

    @Test
    fun `delete user should fail given invalid id`() {
        val exception = assertFailsWith<ResponseStatusException> {
            userServiceImpl.deleteUser(Constants.INVALID_ID)
        }
        val expectedError = "404 NOT_FOUND \"User with id: ${Constants.INVALID_ID} does not exist\""

        assertEquals(expectedError, exception.message)
    }
}