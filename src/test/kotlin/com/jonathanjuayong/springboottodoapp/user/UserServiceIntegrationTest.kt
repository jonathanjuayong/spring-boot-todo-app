package com.jonathanjuayong.springboottodoapp.user

import com.fasterxml.jackson.databind.ObjectMapper
import com.jonathanjuayong.springboottodoapp.SpringBootTodoAppApplicationTests
import com.jonathanjuayong.springboottodoapp.repositories.UserRepository
import com.jonathanjuayong.springboottodoapp.utils.EntityGenerator
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*

@AutoConfigureMockMvc
class UserServiceIntegrationTest : SpringBootTodoAppApplicationTests() {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var userRepository: UserRepository

    @BeforeEach
    fun setUp() {
        userRepository.deleteAll()
    }

    @Test
    fun `get all users should return all users`() {
        val user1 = EntityGenerator.createRandomUser()
        val user2 = EntityGenerator.createRandomUser()
        userRepository.saveAll(listOf(user1, user2))
        mockMvc.get("/api/users") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            jsonPath("$.length()") { value(2) }
        }
    }

    @Test
    fun `get user by id should return user given valid id`() {
        val user = EntityGenerator.createRandomUser()
        val savedUser = userRepository.save(user)

        mockMvc.get("/api/users/${savedUser.id}") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            jsonPath("$.id") { value(savedUser.id) }
        }
    }

    @Test
    fun `create user should return user given valid id`() {
        val user = EntityGenerator.createRandomUser()

        mockMvc.post("/api/users") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(user)
        }.andExpect {
            status { isOk() }
            jsonPath("$") { isNotEmpty() }
        }
    }

    @Test
    fun `update user should return updated user given valid id`() {
        val user = EntityGenerator.createRandomUser()
        val savedUser = userRepository.save(user)
        val newBody = savedUser.copy(
            name = "myNewName",
            email = "mynewemail@gmail.com"
        )

        mockMvc.put("/api/users/${savedUser.id}") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(newBody)
        }.andExpect {
            status { isOk() }
            jsonPath("$.id") { value(newBody.id) }
            jsonPath("$.name") { value(newBody.name) }
            jsonPath("$.email") { value(newBody.email) }
        }
    }

    @Test
    fun `delete user should remove the user given valid id`() {
        val user = EntityGenerator.createRandomUser()
        val savedUser = userRepository.save(user)

        mockMvc.delete("/api/users/${savedUser.id}") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            content { string("") }
        }
    }
}