package com.jonathanjuayong.springboottodoapp.serviceImpl

import com.jonathanjuayong.springboottodoapp.entities.User
import com.jonathanjuayong.springboottodoapp.repositories.UserRepository
import com.jonathanjuayong.springboottodoapp.services.UserService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class UserServiceImpl(private val userRepository: UserRepository) : UserService {
    override fun getAllUsers(): List<User> =
        userRepository.findAll().toList()

    override fun getUserById(id: Long): User =
        userRepository
            .findById(id)
            .orElseThrow {
                ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist")
            }

    override fun createUser(user: User): User =
        if (userRepository.doesEmailAlreadyExist(user.email))
            throw ResponseStatusException(
                HttpStatus.CONFLICT,
                "Email ${user.email} already exists"
            )
        else
            userRepository.save(user)

    override fun updateUser(id: Long, body: User): User {
        val user = userRepository
            .findById(id)
            .orElseThrow {
                ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist")
            }

        return userRepository.save(
            body.copy(
                id = user.id
            )
        )
    }

    override fun deleteUser(id: Long) {
        val user = userRepository
            .findById(id)
            .orElseThrow {
                ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id does not exist")
            }

        userRepository.delete(user)
    }

}