package com.jonathanjuayong.springboottodoapp.utils

import com.jonathanjuayong.springboottodoapp.entities.Todo
import com.jonathanjuayong.springboottodoapp.entities.User
import com.jonathanjuayong.springboottodoapp.repositories.UserRepository

object EntityGenerator {
    private val usernameAndEmailMap = mapOf(
        "mscrimgeour0" to "apearton0@latimes.com",
        "aarblaster1" to "mbettleson1@discovery.com",
        "ttolomelli2" to "aivachyov2@istockphoto.com",
        "ghazeman3" to "mlippini3@sun.com",
        "fedden4" to "rmacilhench4@wikia.com",
        "jwilkison5" to "wgrimme5@eepurl.com",
        "spocknell6" to "talthrope6@harvard.edu",
        "callbut7" to "nshenley7@homestead.com",
        "tdannett8" to "cpendergast8@hostgator.com",
        "iheimes9" to "kioannou9@twitter.com"
    )

    private val todoMap = mapOf(
        "clean up the dishes" to "clean up the dishes after lunch and dinner",
        "study kotlin" to "review all notes from today's discussion",
        "take out the trash" to "gather all the trash from all the rooms and throw it outside",
        "pay credit card bills" to "compute and verify all credit card payments and pay them online",
        "organize files" to "create separate folders for different files and sort them",
        "exercise" to "do 6 x 10 pull ups and 30 mins jumping rope",
        "buy groceries" to "go to the mall and buy groceries good for the whole week",
        "clean up the bathroom" to "user bleach to clean up the toilet and shower",
        "do the laundry" to "separate the coloreds and put them in the laundry for washing",
        "take vitamins" to "drink multivitamins and other supplements before going to bed",
        "review tasks" to "tick off completed tasks and follow up on pending ones",
    )

    fun createRandomUser(): User {
        val name = usernameAndEmailMap.keys.random()
        val email = usernameAndEmailMap[name]!!
        return User(
            name = name,
            email = email
        )
    }

    fun createRandomTodo(user: User): Todo {
        val title = todoMap.keys.random()
        val description = todoMap[title]!!
        return Todo(
            title = title,
            description = description,
            user = user
        )
    }

    fun <T> initializeUserRelatedData(
        userRepository: UserRepository,
        entityGenerator: (User) -> T
    ): Pair<User, T> {
        val user = createRandomUser()
        val savedUser = userRepository.save(user)
        val entity = entityGenerator(user)
        return Pair(savedUser, entity)
    }
}