package com.jonathanjuayong.springboottodoapp.services

import com.jonathanjuayong.springboottodoapp.entities.Todo

interface TodoService {
    fun getAllTodosByUser(userId: Long): List<Todo>
    fun getTodoById(id: Long): Todo
    fun createTodo(userId: Long, body: Todo): Todo
    fun updateTodo(id: Long, body: Todo): Todo
    fun deleteTodo(id: Long)
}