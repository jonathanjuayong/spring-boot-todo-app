package com.jonathanjuayong.springboottodoapp.services

import com.jonathanjuayong.springboottodoapp.entities.User

interface UserService {
    fun getAllUsers(): List<User>
    fun getUserById(id: Long): User
    fun createUser(user: User): User
    fun updateUser(id: Long, body: User): User
    fun deleteUser(id: Long)
}