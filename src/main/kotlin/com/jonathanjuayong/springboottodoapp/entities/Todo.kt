package com.jonathanjuayong.springboottodoapp.entities

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table
data class Todo(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @Column(
        nullable = false,
        updatable = true,
        name = "title"
    )
    val title: String = "",

    @Column(
        nullable = false,
        updatable = true,
        name = "description"
    )
    val description: String = "",

    @Column(
        nullable = false,
        updatable = true,
        name = "isCompleted"
    )
    val isCompleted: Boolean = false,

    @Column(
        nullable = false,
        updatable = true,
        name = "dateCreated"
    )
    val dateCreated: LocalDate = LocalDate.now(),

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", referencedColumnName = "id")
    val user: User

    )