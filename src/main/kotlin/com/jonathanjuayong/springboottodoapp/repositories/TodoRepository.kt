package com.jonathanjuayong.springboottodoapp.repositories

import com.jonathanjuayong.springboottodoapp.entities.Todo
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface TodoRepository: CrudRepository<Todo, Long> {
    @Query(
        """
            SELECT t
            FROM Todo t
            WHERE t.user.id = :userId
        """
    )
    fun getAllUserTodos(userId: Long): List<Todo>

    @Query(
        """
            SELECT CASE WHEN COUNT(t) > 0 THEN TRUE ELSE FALSE END
            FROM Todo t
            WHERE t.title = :title            
        """
    )
    fun isTitleDuplicate(title: String): Boolean
}