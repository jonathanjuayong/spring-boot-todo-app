package com.jonathanjuayong.springboottodoapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringBootTodoAppApplication

fun main(args: Array<String>) {
	runApplication<SpringBootTodoAppApplication>(*args)
}
