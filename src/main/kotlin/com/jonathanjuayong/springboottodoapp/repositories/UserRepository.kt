package com.jonathanjuayong.springboottodoapp.repositories

import com.jonathanjuayong.springboottodoapp.entities.User
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<User, Long> {
    @Query(
        """
            SELECT CASE WHEN COUNT(u) > 0 THEN TRUE ELSE FALSE END
            FROM User u
            WHERE u.email = :email
        """
    )
    fun doesEmailAlreadyExist(email: String): Boolean
}